# Gen AI Summarizer

Gen AI Summarizer is an application for Enhancing Productivity: (Utilizing AI to improve knowledge sharing) This solution will transcribe the customer service agent call in real-time, as a part of the call rap-up procedure customer agent must manually summarize the conversation that lasts between 20 min to 2 hours, losing valuable and costly time. The proposed solution will save the time of the agents, allowing them to resolve more customer queries and concerns.  Solution:  Integrate the ChatGPT with the pipeline to create the summary of the full transcript of the customer-agent interaction.  Strategic priority focus: -  This idea connects to -Driving customer values via expert solutions. 

## Installation

Use the package manager [Anaconda](https://www.anaconda.com/) to create a Python environment and install required packages using the Pipfile provided in the repo.

```bash
$ conda create -n openai python=3.11
$ conda activate openai
$ pip install pipenv
```

Make sure that your current working directory contains the two files Pipfile and Pipfile.lock, then run the below commands

```bash
$ pipenv install
$ pipenv shell
```

## Usage

Make sure that the environment created by pipenv is activated then run the below command

```bash
$ streamlit run main.py
```
Now you can access the application at this [link](http://localhost:8501/)